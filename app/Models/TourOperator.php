<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourOperator extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $fillable = [
		"name",
		"logo",
		"hero_image",
		"headquarters",
		"website",
		"facebook_link",
		"youtube_link",
		"twitter_link",
		"instagram_link",
		"linkedin_link",
		"pinterest_link",
		"company_contact",
		"company_email",
		"company_address",
		"description",
		"contact_person",
		"awards",
		"google_review_link",
		"trip_advisor_review_link",
		"safari_bookings_review_link",
		"trust_pilot_review_link",
		'accreditations',
		'slug',
		'isReviewed',
		'isVerified',
		"public_id",
		"tour_types"
    ];

    public function countries() {
        return $this->hasMany(Country::class);
    }

    /*public function tour_types() {
        return $this->belongsToMany(TourType::class);
    }*/
}