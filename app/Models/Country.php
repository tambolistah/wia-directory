<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model {
    use HasFactory;

    public $fillable = [
		"country",
		"tour_operator_id",
	];

    public function tour_operators() {
        return $this->belongsTo(TourOperator::class);
    }
}