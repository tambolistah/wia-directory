<?php

namespace App\Http\Controllers;

use App\Models\TourOperator;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TourOperatorController extends Controller {
    
    public function home(Request $request) {

        $input_data = $request->input();

        if($request->method() == "POST") {
            $request->merge([
                'page' => 1,
            ]);
        }

        if(!empty($input_data)) {
            
            $tour_operators = new TourOperator();
            
            if(isset($input_data['selected_mode']) && $input_data['selected_mode'] == "company") {
                if(isset($input_data['company_name']) && !empty($input_data['company_name'])) {
                    $tour_operators = $tour_operators->where("name", 'like', '%'.$input_data['company_name'].'%');
                }
            }


            if(isset($input_data['selected_mode']) && $input_data['selected_mode'] == "country") {
                
                if(isset($input_data['countries'])) {
                    if(!is_array($input_data['countries'])) {
                        $input_data['countries'] = explode(",", $input_data['countries']);
                    }
                }

                if(isset($input_data['countries']) && !empty($input_data['countries'])) {
                    $countries = Country::select('tour_operator_id')->whereIn("country", $input_data['countries'])->get();

                    $flatten_operator_ids = array();
                    foreach($countries as $country) {
                        $flatten_operator_ids[] = $country->tour_operator_id;
                    }

                    $tour_operators = $tour_operators->whereIn("id", $flatten_operator_ids);
                }
            }


            if(isset($input_data['with_reviews']) && $input_data['with_reviews'] == 1) {
                $tour_operators = $tour_operators->where("isReviewed", true);
            }

            if(isset($input_data['is_verified']) && $input_data['is_verified'] == 1) {
                $tour_operators = $tour_operators->where("isVerified", true);
            }

            if(isset($input_data['with_accreditations']) && $input_data['with_accreditations'] == 1) {
                $tour_operators = $tour_operators->whereNotNull('accreditations');
            }

            $tour_operators = $tour_operators->paginate(50);
        } else {
            $tour_operators = TourOperator::paginate(50);
        }

        return view("home", ["tour_operators" => $tour_operators, "input_data" => $input_data]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function index() {
        $tour_operators = TourOperator::paginate(10);
        return view("tour_operator/index", ["tour_operators" => $tour_operators]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function create() {
        return view("tour_operator/create");
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function store(Request $request) {

        $random = Str::random(30);
        $post_data = $request->input();

        $hero_image = $request->file("hero_image");
        $logo_image = $request->file("logo");

        //Move Uploaded File
        $destinationPath = "uploads/".$random . '-uploads';

        $hero_image_name = "hero-".$hero_image->getClientOriginalName();
        $logo_image_name = "logo-".$logo_image->getClientOriginalName();

        $hero_image->move($destinationPath,$hero_image_name);
        $logo_image->move($destinationPath,$logo_image_name);

        $post_data['hero_image'] = $hero_image_name;
        $post_data['logo'] = $logo_image_name;
        $post_data['public_id'] = $random;

        /*$award_data = array();
        if($post_data['world_travel_award'] == 1) {
            array_push($award_data, "world_travel_award");
        }

        if($post_data['world_travel_award'] == 1) {
            array_push($award_data, "trip_advisor_award");
        }*/

        /*$post_data['awards'] = implode(",", $award_data);*/
        $created_tour_operator = TourOperator::create($post_data);

        if(count($request->input('countries')) > 0) {
            $countries = $request->input('countries');
            
            foreach($countries as $country) {
                
                $to_create_countries = array(
                    "country" => $country,
                    "tour_operator_id" => $created_tour_operator->id
                );

                Country::create($to_create_countries);

            }
        }

        return redirect()->to('/tour_operator');
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function show(TourOperator $tourOperator) {
        //
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function directory($slug) {
        $tour_operator = TourOperator::where("slug", $slug)->first();

        $countries = Country::select("country")->where("tour_operator_id", $tour_operator['id'])->get();

        $flatten_countries = array();
        foreach($countries as $country) {
            $flatten_countries[] = $country->country;
        }

        $tour_operator['countries'] = implode(", ", $flatten_countries);
        $data['tour_operator'] = $tour_operator;
        return view("directory/operator", ["data" => $data]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function edit(TourOperator $tourOperator) {

        $countries = Country::select("country")->where("tour_operator_id", $tourOperator->id)->get();

        $flatten_countries = array();
        foreach($countries as $country) {
            $flatten_countries[] = $country->country;
        }

        $data = array(
            "operator_countries"    => $flatten_countries,
            "tourOperator"          => $tourOperator
        );

        return view("tour_operator/edit", ["data" => $data]);
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function update(Request $request, TourOperator $tourOperator) {
        $hero_image = $request->file("hero_image");
        $logo_image = $request->file("logo");

        $post_data = $request->input();

        //Move Uploaded File
        $destinationPath = "uploads/".$tourOperator->public_id . '-uploads';
        
        if($request->hasFile('hero_image')) {
            $new_hero_file = "hero-".$hero_image->getClientOriginalName();

            $hero_image = $request->file("hero_image");    
            $hero_image->move($destinationPath,$new_hero_file);

            $current_hero_loc = public_path() . "/uploads/" . $tourOperator->public_id ."-uploads/". $tourOperator->hero_image;
            
            if (file_exists($current_hero_loc)) {
                unlink($current_hero_loc);
            }

            $tourOperator->hero_image = $new_hero_file;
        }

        if($request->hasFile('logo')) {
            $new_logo_file = "logo-".$logo_image->getClientOriginalName();

            $logo_image = $request->file("logo");
            $logo_image->move($destinationPath,$new_logo_file);

            $current_logo_loc = public_path() . "/uploads/" . $tourOperator->public_id ."-uploads/". $tourOperator->logo;

            if (file_exists($current_logo_loc)) {
                unlink($current_logo_loc);
            }

            $tourOperator->logo = $new_logo_file;
        }



        /*$award_data = array();
        if($post_data['world_travel_award'] == 1) {
            array_push($award_data, "world_travel_award");
        }

        if($post_data['world_travel_award'] == 1) {
            array_push($award_data, "trip_advisor_award");
        }

        $post_data['awards'] = implode(",", $award_data);*/
        
        $tourOperator->fill($post_data);
        $tourOperator->save();

        Country::where("tour_operator_id", $tourOperator->id)->delete();
        
        if(count($request->input('countries')) > 0) {
            $countries = $request->input('countries');
            foreach($countries as $country) {
                $to_create_countries = array(
                    "country" => $country,
                    "tour_operator_id" => $tourOperator->id
                );

                Country::create($to_create_countries);
            }
        }

        return redirect()->to('/tour_operator');
    }

    /* --------------------------------------------------------------------------- FUNCTION SEPARATOR --------------------------------------------------------------------------- */

    public function destroy(TourOperator $tourOperator) {
        $tourOperator->delete();
    }
}
