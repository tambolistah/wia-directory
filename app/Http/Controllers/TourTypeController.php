<?php

namespace App\Http\Controllers;

use App\Models\TourType;
use Illuminate\Http\Request;

class TourTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TourType  $tourType
     * @return \Illuminate\Http\Response
     */
    public function show(TourType $tourType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TourType  $tourType
     * @return \Illuminate\Http\Response
     */
    public function edit(TourType $tourType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TourType  $tourType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourType $tourType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TourType  $tourType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourType $tourType)
    {
        //
    }
}
