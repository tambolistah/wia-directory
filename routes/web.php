<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\TourOperatorController::class, 'home'])->name('home');
Route::post('/', [App\Http\Controllers\TourOperatorController::class, 'home'])->name('home');

Auth::routes();

/*Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');*/

Route::group(['middleware' => ['auth']], function() {
	Route::resource('tour_operator', 'App\Http\Controllers\TourOperatorController');
});

Route::get('/directory/{id}', 'App\Http\Controllers\TourOperatorController@directory');