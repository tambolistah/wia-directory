<header>
	<nav id="topMainNav">
		<div class="navContentHolder">
			<div id="logoHolder">
				<a class="mainNavLogo" href="/">
					<span class="imageLogoWrapper">
						<img class="for_default" src="/logo.png" alt="African Safaris">
					</span>
				</a>
			</div>

			<div id="manNavWrapper">

				<div class="navigation-content-wrapper">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fa fa-bars">&nbsp;</i>
					</button>

					<nav id="main-menu-nav" class="navbar navbar-expand-lg navbar-light bg-light">
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul id="mainNavMenu">
								<div class="menu-main-menu-container">

									<ul id="menu-main-menu" class="full">
										<li id="menu-item-117" class="main-highlight-brown menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-117"><a href="https://whileinafrica.com" aria-current="page"><span class="screen-reader-text"><< Back to the Main Site</span></a></li>
										<li id="menu-item-117" class="main-highlight-brown menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-117"><a href="https://whileinafrica.com/tour-operator-sign-up" aria-current="page"><span class="screen-reader-text">Register</span></a></li>
										<!-- <li id="menu-item-142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-142"><a href="/"><span class="screen-reader-text">Safari &amp; Tours</span></a></li> -->
									<!-- <li id="menu-item-16498" class="menu-item menu-item-type-post_type menu-item-object-experience menu-item-16498"><a href="/experience/group-joining/"><span class="screen-reader-text">Group Joining</span></a></li>
									<li id="menu-item-114" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114"><a href="/experiences/"><span class="screen-reader-text">Experiences</span></a></li>
									
									<li id="menu-item-143" class="parent-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-143">
										<a href="#">
											<span class="screen-reader-text">Destinations</span>
										</a>

										<ul class="sub-menu">
											<li id="menu-item-11227" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-10773 current_page_item menu-item-11227"><a href="https://whileinafrica.com/tanzania/" aria-current="page">Tanzania</a></li>
											<li id="menu-item-11228" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11228"><a href="https://whileinafrica.com/kenya/">Kenya</a></li>
											<li id="menu-item-11184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11184"><a href="https://whileinafrica.com/uganda/">Uganda</a></li>
											<li id="menu-item-11183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11183"><a href="https://whileinafrica.com/ethiopia/">Ethiopia</a></li>
											<li id="menu-item-11178" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11178"><a href="https://whileinafrica.com/morocco/">Morocco</a></li>
											<li id="menu-item-11180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11180"><a href="https://whileinafrica.com/botswana/">Botswana</a></li>
											<li id="menu-item-11182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11182"><a href="https://whileinafrica.com/south-africa/">South Africa</a></li>
											<li id="menu-item-11181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11181"><a href="https://whileinafrica.com/namibia/">Namibia</a></li>
											<li id="menu-item-11176" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11176"><a href="https://whileinafrica.com/seychelles/">Seychelles</a></li>
											<li id="menu-item-11179" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11179"><a href="https://whileinafrica.com/mozambique/">Mozambique</a></li>
											<li id="menu-item-11177" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11177"><a href="https://whileinafrica.com/mauritania/">Mauritania</a></li>
											<li id="menu-item-11175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11175"><a href="https://whileinafrica.com/zambia/">Zambia</a></li>
											<li id="menu-item-11174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11174"><a href="https://whileinafrica.com/zimbabwe/">Zimbabwe</a></li>
										</ul>
									</li>

									<li id="menu-item-220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220"><a href="/when-to-travel-page/"><span class="screen-reader-text">When To Travel</span></a></li>
									<li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115"><a href="https://whileinafrica.com/contact-us/"><span class="screen-reader-text">Contact Us</span></a></li> -->
								</ul></div>                  
							</ul>
						</div>

					</nav>
				</div>

			</div>
		</div>
	</nav>
</header>