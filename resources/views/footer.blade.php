<footer>
	<div class="limiter">
		<div class="row">
			<div id="footer-menu" class="col-md-2 col-12">
				<img class="for_default" src="/logo.png" alt="African Safaris">

				<div class="footer-menu-wrapper">
					<ul>
						<li>
							<a href="https://whileinafrica.com/about-us/">About Us</a>
						</li>
						<li>
							<a href="https://whileinafrica.com/contribute-blog/">Contribute Blog</a>
						</li>
						<li>
							<a href="https://whileinafrica.com/crossing-borders/">Crossing Borders</a>
						</li>
						<li>
							<a href="https://whileinafrica.com/tourist-visa-guide/">Tourist Visa Guide</a>
						</li>

						<li>
							<a href="https://whileinafrica.com/travel-operators/">Travel Operators</a>
						</li>

						<li>
							<a href="https://whileinafrica.com/sustainable-travels/">Sustainable Travels</a>
						</li>

						<li>
							<a href="https://whileinafrica.com/safari-destinations/">Safari Destinations</a>
						</li>
					</ul>
				</div>
			</div>

			<div id="african_countries" class="col-md-10 col-12">
				
				<h5>Africa Continents and Countries</h5>

				<div class="row">
					<div class="col-md-3 col-12">
						<div class="row">
							<div class="col-md-6 col-12">
								<h6>North Africa</h6>

								<div class="country-list-wrapper">
									<ul>
										<li>Algeria</li>
										<li>Egypt</li>
										<li>Libya</li>
										<li>Morocco</li>
										<li>Sudan</li>
										<li>Tunisia</li>
									</ul>
								</div>
							</div>

							<div class="col-md-6 col-12">
								<h6>Southern Africa</h6>

								<div class="country-list-wrapper">
									<ul>
										<li>Botswana</li>
										<li>Eswatini</li>
										<li>Lesotho</li>
										<li>Namibia</li>
										<li>South Africa</li>
										<li>South Sudan</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-12">
						<h6>Eastern Africa</h6>

						<div class="country-list-wrapper">
							<div class="row">
								<div class="col-md-4 col-12">
									<ul>
										<li>Burundi</li>
										<li>Comoros</li>
										<li>Djibouti</li>
										<li>Eritrea</li>
										<li>Ethiopia</li>
										<li>Kenya</li>
									</ul>
								</div>

								<div class="col-md-4 col-12">
									<ul>
										<li>Madagascar</li>
										<li>Malawi</li>
										<li>Mauritius</li>
										<li>Mozambique</li>
										<li>Rwanda</li>
										<li>Seychelles</li>		
									</ul>
								</div>

								<div class="col-md-4 col-12">
									<ul>
										<li>Somalia</li>
										<li>Tanzania</li>
										<li>Uganda</li>
										<li>Zambia</li>
										<li>Zimbabwe</li>
									</ul>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-3 col-12">
						<h6>West Africa</h6>
						<div class="country-list-wrapper">
							<div class="row">
								<div class="col-md-4">
									<ul>
										<li>Benin</li>
										<li>Burkina Faso</li>
										<li>Cabo Verde</li>
										<li>Cote d'Ivoire</li>
										<li>Gambia</li>
										<li>Guinea</li>
									</ul>
								</div>
								<div class="col-md-4">
									<ul>
										<li>Guinea-Bissau</li>
										<li>Liberia</li>
										<li>Mali</li>
										<li>Mauritania</li>
										<li>Niger</li>
										<li>Nigeria</li>
									</ul>
								</div>
								<div class="col-md-4">
									<ul>
										<li>Senegal</li>
										<li>Togo</li>
									</ul>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-2 col-12">
						<h6>Central Africa</h6>

						<div class="country-list-wrapper">
							
							<div class="row">
								<div class="col-md-6 col-12">
									<ul>
										<li>Angola</li>
										<li>Cameroon</li>
										<li>Central African Republic</li>
										<li>Chad</li>
										<li>Democratic Republic of the Congo</li>
									</ul>
								</div>
								<div class="col-md-6 col-12">
									<ul>
										<li>Republic of the Congo</li>
										<li>Equatorial Guinea</li>
										<li>Gabon</li>
										<li>São Tomé and Príncipe</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<?php
		if(!isset($_COOKIE["removeStickyFooter"])) {
		  ?>
		  	<div id="sticky-footer">
				<div id="sticky-footer-content">
					<div id="text-content-wrapper">
						We are still under development but feel free navigate our website and tell us your feedback. <a href="https://whileinafrica.com/tour-operator-sign-up/"><span class="orange-highlight">You can also send us your company details</span></a> to be included in our list.

						<div id="sitcky-closer">
							<i class="fa fa-times"></i>
						</div>
					</div>
					<!-- <div id="button-content-wrapper">
						<a class="orange-button filter-trigger" id="orange-register" href="https://whileinafrica.com/tour-operator-sign-up/">REGISTER</a>
					</div> -->
				</div>
			</div>
		  <?php
		}
	?>

</footer>

<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript">
	
	$(document).ready( function () {

		// setCookie('removeStickyFooter','1','30');
		// getCookie('test');

		$('#sitcky-closer').click ( function () {
			console.log('clicked');
			$('#sticky-footer').remove();
			setCookie('removeStickyFooter','1','30');
		});

		function setCookie(key, value, expiry) {
	        var expires = new Date();
	        expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
	        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
	    }

	    function getCookie(key) {
	        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	        return keyValue ? keyValue[2] : null;
	    }

	});

</script>