@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Update Tour Operator</h1>
@stop

@section('content')
    
    <?php
        $tourOperator = $data["tourOperator"];
    ?>

    <form method="POST" enctype="multipart/form-data" action="/tour_operator/{{ $tourOperator->id }}">
        @method('patch')
        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
        <input type="hidden" name="id" value="{{ $tourOperator->id }}">
        <div id="group_performance">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h4>
                                Basic Information
                            </h4>

                            <div class="form-group">
                                <label>
                                    Logo
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#logoModal">
                                        View Current Logo
                                    </button>
                                </label>
                                <input type="file" class="form-control no-border" name="logo" placeholder="Operator Logo" />
                                <!-- Modal -->
                                <div class="modal fade" id="logoModal" tabindex="-1" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Operator Logo</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <center>
                                            <img src="/uploads/<?php echo $tourOperator->public_id ."-uploads/". $tourOperator->logo; ?>" />
                                        </center>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>
                                    Featured Image
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#heroModal">
                                        View Current Featured Image
                                    </button>
                                </label>
                                <input type="file" class="form-control no-border" name="hero_image" placeholder="Operator Hero Image" />

                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-lg" id="heroModal" tabindex="-1" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Operator Featured Image</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <center>
                                            <img src="/uploads/<?php echo $tourOperator->public_id ."-uploads/". $tourOperator->hero_image; ?>" />
                                        </center>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" value="{{ $tourOperator->name }}" placeholder="Operator Name" />
                            </div>

                            
                            <div class="form-group">
                                <input type="text" class="form-control" name="slug" value="{{ $tourOperator->name }}" id="slug" placeholder="Slug" />
                            </div>

                            <div class="form-group">
                                <textarea class="form-control" name="headquarters" placeholder="Operator Headquarter">{{ $tourOperator->headquarters }}</textarea>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="website" value="{{ $tourOperator->website }}" placeholder="Operator Website" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="tour_types" value="{{ $tourOperator->tour_types }}" placeholder="Tour Types" />
                            </div>

                            <hr />

                            <h4>Social Media</h4>


                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-facebook-f"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="facebook_link" value="{{ $tourOperator->facebook_link }}" placeholder="Facebook Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="youtube_link" value="{{ $tourOperator->youtube_link }}" placeholder="Youtube Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="twitter_link" value="{{ $tourOperator->youtube_link }}" placeholder="Twitter Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="instagram_link" value="{{ $tourOperator->instagram_link }}" placeholder="Instagram Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-linkedin"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="linkedin_link" value="{{ $tourOperator->linkedin_link }}" placeholder="Linkedin Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-pinterest"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="pinterest_link" value="{{ $tourOperator->pinterest_link }}"  placeholder="Pinterest Link">
                                  </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Contact Information</h4>

                            <div class="row">

                                <div class="col-12">

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_contact" value="{{ $tourOperator->company_contact }}" placeholder="Contact Numbers" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_email" value="{{ $tourOperator->company_email }}" placeholder="Operator Email" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_address" value="{{ $tourOperator->company_address }}" placeholder="Operator Address / PO Box" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="contact_person" value="{{ $tourOperator->contact_person }}" placeholder="Contact Person" />
                                    </div>

                                </div>
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-12">
                                    <label>Operator Description</label>
                                    <div class="form-group">
                                        <textarea name="description" class="editor">{!! $tourOperator->description !!}</textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12">
                                    <label>Accreditations</label>
                                    <div class="form-group">
                                        <textarea name="accreditations" class="editor">{!! $tourOperator->accreditations !!}</textarea>
                                    </div>
                                </div>
                            </div>

                            <hr />
                            
                            <!-- <h4>Operator Awards</h4> -->

                            <?php
                                $awards = explode(",",$tourOperator->awards);
                            ?>

                            <!-- <div class="row">
                                <div class="col-12">
                                    <br />
                                    <div class="custom-control custom-checkbox">
                                        <input name="world_travel_award" type="hidden" value="0">
                                        <input <?php if(in_array("world_travel_award", $awards)) { echo "checked"; } ?> name="world_travel_award" id="world_travel_award" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="world_travel_award">World Travel Award</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input name="trip_advisor_award" type="hidden" value="0">
                                        <input <?php if(in_array("trip_advisor_award", $awards)) { echo "checked"; } ?> name="trip_advisor_award" id="trip_advisor_award" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="trip_advisor_award">Trip Advisor Certificate of Excellence Awardee</label>
                                    </div>
                                </div>
                            </div>

                            <hr /> -->
                            
                            <h4>Filters</h4>

                            <div class="row">
                                <div class="col-12">
                                    <br />
                                    <div class="custom-control custom-checkbox">
                                        <input name="isReviewed" type="hidden" value="0">
                                        <input <?php if($tourOperator->isReviewed) { echo "checked"; } ?> name="isReviewed" id="isReviewed" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="isReviewed">Reviewed</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input name="isVerified" type="hidden" value="0">
                                        <input <?php if($tourOperator->isVerified) { echo "checked"; } ?> name="isVerified" id="isVerified" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="isVerified">Verified</label>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Review Links or Embed Code</h4>

                            <div class="row">
                                <div class="col-12">

                                    <div class="row">
                                        <div class="col-12">
                                            <label>Google Review Link</label>
                                            <div class="form-group">
                                                <textarea name="google_review_link" class="form-control" placeholder="Google Review Link">{!! $tourOperator->google_review_link !!}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Trip Advisor Review Link</label>
                                            <div class="form-group">
                                                <textarea name="trip_advisor_review_link" class="form-control" placeholder="Trip Advisor Review Link">{!! $tourOperator->trip_advisor_review_link !!}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Safari Booking Review Link</label>
                                            <div class="form-group">
                                                <textarea name="safari_bookings_review_link" class="form-control" placeholder="Safari Booking Review Link">{!! $tourOperator->safari_bookings_review_link !!}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Trust Pilot Review Link</label>
                                            <div class="form-group">
                                                <textarea name="trust_pilot_review_link" class="form-control" placeholder="Trust Pilot Review Link">{!! $tourOperator->trust_pilot_review_link !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Countries</h4>

                            <div class="search-toggle-wrapper">
                                <div class="form-group">
                                    <select name="countries[]" data-placeholder="Operating in following countries" multiple class="form-control chosen-select text-left">
                                        <?php
                                            foreach(Config::get('public.countries') as $country) {
                                                if(in_array($country, $data["operator_countries"])) {
                                                    echo "<option selected value='$country'>$country</option>";
                                                } else {
                                                    echo "<option value='$country'>$country</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <hr />

                            <input type="submit" class="btn btn-success" value="Update Operator">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop


@section('adminlte_js')
    @parent

    <link rel="stylesheet" type="text/css" href="/assets/chosen/chosen.min.css" />
    <script type="text/javascript" src="/assets/chosen/chosen.jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready( function () {

            $(".chosen-select").chosen();

            function convertToSlug(Text) {
                return Text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    ;
            }

            $('#name').keyup( function () {
                var slug = convertToSlug($(this).val());
                $('#slug').val(slug);
            });
        });
    </script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: 'textarea.editor',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste",
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      });
    </script>
@stop