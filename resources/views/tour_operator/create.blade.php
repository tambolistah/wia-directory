@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Create Tour Operator</h1>
@stop

@section('content')
    
    <form method="POST" enctype="multipart/form-data" action="/tour_operator">
        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
        <div id="group_performance">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            
                            <h4>Basic Information</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Operator Name" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Slug" />
                            </div>

                            <div class="form-group">
                                <label>Logo</label>
                                <input type="file" class="form-control no-border" name="logo" placeholder="Operator Logo" />
                            </div>

                            <div class="form-group">
                                <label>Featured Image</label>
                                <input type="file" class="form-control no-border" name="hero_image" placeholder="Operator Hero Image" />
                            </div>

                            <div class="form-group">
                                <textarea class="form-control" name="headquarters" placeholder="Operator Headquarter"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="website" placeholder="Operator Website" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="tour_types" placeholder="Tour Types" />
                            </div>

                            <hr />

                            <h4>Social Media</h4>


                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-facebook-f"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="facebook_link" placeholder="Facebook Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="youtube_link" placeholder="Youtube Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="twitter_link" placeholder="Twitter Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="instagram_link" placeholder="Instagram Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-linkedin"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="linkedin_link" placeholder="Linkedin Link">
                                  </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroupPrepend2">
                                        <i class="fab fa-pinterest"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" id="validationDefaultUsername" name="pinterest_link" placeholder="Pinterest Link">
                                  </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Contact Information</h4>

                            <div class="row">

                                <div class="col-12">

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_contact" placeholder="Contact Numbers" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_email" placeholder="Operator Email" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_address" placeholder="Operator Address / PO Box" />
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="contact_person" placeholder="Contact Person" />
                                    </div>

                                </div>
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-12">
                                    <label>Operator Description</label>
                                    <div class="form-group">
                                        <textarea name="description" class="editor"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <label>Accreditations</label>
                                    <div class="form-group">
                                        <textarea name="accreditations" class="editor"></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr />
                            
                            <!-- <h4>Operator Awards</h4>

                            <div class="row">
                                <div class="col-12">
                                    <br />
                                    <div class="custom-control custom-checkbox">
                                        <input name="world_travel_award" type="hidden" value="0">
                                        <input name="world_travel_award" id="world_travel_award" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="world_travel_award">World Travel Award</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input name="trip_advisor_award" type="hidden" value="0">
                                        <input name="trip_advisor_award" id="trip_advisor_award" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="trip_advisor_award">Trip Advisor Certificate of Excellence Awardee</label>
                                    </div>
                                </div>
                            </div>

                            <hr /> -->
                            
                            <h4>Filters</h4>

                            <div class="row">
                                <div class="col-12">
                                    <br />
                                    <div class="custom-control custom-checkbox">
                                        <input name="isReviewed" type="hidden" value="0">
                                        <input name="isReviewed" id="isReviewed" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="isReviewed">Reviewed</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input name="isVerified" type="hidden" value="0">
                                        <input name="isVerified" id="isVerified" type="checkbox" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="isVerified">Verified</label>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Review Links or Embed Code</h4>

                            <div class="row">
                                <div class="col-12">

                                    <div class="row">
                                        <div class="col-12">
                                            <label>Google Review Link</label>
                                            <div class="form-group">
                                                <textarea name="google_review_link" class="form-control" placeholder="Google Review Link"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Trip Advisor Review Link</label>
                                            <div class="form-group">
                                                <textarea name="trip_advisor_review_link" class="form-control" placeholder="Trip Advisor Review Link"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Safari Booking Review Link</label>
                                            <div class="form-group">
                                                <textarea name="safari_bookings_review_link" class="form-control" placeholder="Safari Booking Review Link"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label>Trust Pilot Review Link</label>
                                            <div class="form-group">
                                                <textarea name="trust_pilot_review_link" class="form-control" placeholder="Trust Pilot Review Link"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <h4>Countries</h4>

                            <div class="search-toggle-wrapper">
                                <div class="form-group">
                                    <select name="countries[]" data-placeholder="Operating in following countries" multiple class="form-control chosen-select text-left">
                                        <?php
                                            foreach(Config::get('public.countries') as $country) {
                                                echo "<option value='$country'>$country</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <hr />

                            <input type="submit" class="btn btn-success" value="Create Operator">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop


@section('adminlte_js')
    @parent



    <link rel="stylesheet" type="text/css" href="/assets/chosen/chosen.min.css" />
    <script type="text/javascript" src="/assets/chosen/chosen.jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready( function () {
            $(".chosen-select").chosen();

            function convertToSlug(Text) {
                return Text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    ;
            }

            $('#name').keyup( function () {
                var slug = convertToSlug($(this).val());
                $('#slug').val(slug);
            });
        });
    </script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: 'textarea.editor',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste",
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      });
    </script>
@stop