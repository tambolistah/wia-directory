@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Tour Operator List <a href="/tour_operator/create" class="btn btn-success">Create an Operator</a></h1>
@stop

@section('content')
    
    <div id="formMessageHolder"></div>

    <style>
        .rawLink, .deleteForm { margin-left: 3px; }
        .inline-block { display: inline-block; }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                				<th>Logo</th>
                				<th>Name</th>

                				<th>Headquarters</th>
                				<th>Contact</th>
                				<th>Email</th>
                				<th>Address</th>

                				<th>Actions</th>
                			</tr>
                		</thead>
                		<tbody>
                			<?php foreach($tour_operators as $tour_operator) { ?>
	                			<tr id="{{ $tour_operator->id }}">
	                				<td><img src="/uploads/<?php echo $tour_operator->public_id ."-uploads/". $tour_operator->logo; ?>" /></td>
	                				<td>{{ $tour_operator->name }}</td>

	                				<td>{{ $tour_operator->headquarters }}</td>
	                				<td>{{ $tour_operator->company_contact }}</td>
	                				<td>{{ $tour_operator->company_email }}</td>
	                				<td>{{ $tour_operator->company_address }}</td>

	                				<td width="15%">
	                					<a target="_blank" href="/directory/{{ $tour_operator->slug }}" target="_blank" class="btn btn-success inline-block">View</a>
	                					<a href="/tour_operator/{{ $tour_operator->id }}/edit" class="btn btn-warning inline-block">Edit</a>
                                        <?php echo Form::open(array('url' => 'tour_operator/' . $tour_operator->id, 'class' => 'deleteForm inline-block', 'data-id' => $tour_operator->id)); ?>
                                            <?php echo Form::hidden('_method', 'DELETE'); ?>
                                            <?php echo Form::submit('Delete', array('onclick' => 'if(confirm("Are you sure you want to delete this entry?")) { return true; } event.returnValue = false; return false;', 'class' => 'btn btn-danger deleteTag')); ?>
                                        <?php echo Form::close(); ?>

	                				</td>
	                			</tr>
                			<?php } ?>
                		</tbody>
                	</table>
                </div>
            </div>
        </div>

        <hr />

        <center>
            {{ $tour_operators->links() }}
        </center>

    </div>
    
@stop


@section('adminlte_js')
    @parent

    <script type="text/javascript">
        $(document).ready( function () {
            /* ------------------------------------------ EVENT FUNCTION SEPARATOR ------------------------------------------ */

            $('.deleteForm').submit( function () {
                $('#formMessageHolder').fadeOut();
                $('#formMessageHolder').html('');
                $('#formMessageHolder').fadeIn();
                
                var selected = $(this).attr('data-id');

                $.ajax({
                    url: $(this).attr('action'),
                    method: 'DELETE',
                    data: $(this).serialize(),

                    success: function(result) {
                        $('tr#'+selected).remove();
                        $('#formMessageHolder').html('<div class="alert alert-success">Tour was successfully deleted</div>');
                    }
                });

                return false;
            });
        });
    </script>

@stop