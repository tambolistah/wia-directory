<!DOCTYPE html>
<html>
	@include('headtag', ['title' => 'Home'])

	<body>
		@include('header')

		<?php
			$selected_mode = (isset($input_data['selected_mode'])) ? $input_data['selected_mode'] : "country";
			$countries = (isset($input_data['countries'])) ? implode(",", $input_data['countries']) : array();
		?>

		<form method="POST">
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			<input type="hidden" name="selected_mode" id="selected_mode" value="<?php echo $selected_mode; ?>">
			<div id="hero-search-wrapper">

				<center>
					<h1>Find a tour operator in Africa</h1>
					<div id="hero-subtag">Directory to a number of Tour Operators in Africa</div>
					
					<div id="home-search-wrapper">
						<div id="search-format-switcher-wrapper">
							<div id="country-switcher" data-target="#country-search-wrapper" data-value="country" class="uppercase inline-block switcher-instance active-switch">Country</div>
							<div id="company-switcher" data-target="#company-search-wrapper" data-value="company" class="uppercase inline-block switcher-instance">Company Name</div>
						</div>

						<div id="country-search-wrapper" class="search-toggle-wrapper">							
							<i id="gps-filler" class="fas fa-map-marker-alt">&nbsp;</i>
							<div class="form-group">
								<select name="countries[]" data-placeholder="Which country are you going to?" multiple class="form-control chosen-select text-left">
									<?php
										$config_countries = json_encode(Config::get('public.countries'));
										$sorted_countries = json_decode($config_countries);
										sort($sorted_countries);

										if(isset($input_data['countries'])) {
											foreach($sorted_countries as $country) {
												$selected = (in_array($country, $input_data['countries'])) ? "selected" : null;
												echo "<option $selected value='$country'>$country</option>";
											}
										} else {
											foreach($sorted_countries as $country) {
												echo "<option value='$country'>$country</option>";
											}
										}
									?>
								</select>
							</div>

							<input type="submit" class="orange-button country-search-instance pointer" value="SEARCH" />
						</div>

						<div id="company-search-wrapper" class="hidden search-toggle-wrapper">
							<input type="text" placeholder="Company name" id="company-name-search" name="company_name" value="<?php echo (isset($input_data['company_name'])) ? $input_data['company_name'] : null; ?>" />
							<input type="submit" class="orange-button country-search-instance pointer" value="SEARCH" />
						</div>

					</div>
				</center>

			</div>

			<div class="limiter">
				<div class="main-content-wrapper">

					<div class="row">
						<div id="filter-content-holder" class="col-md-3 col-12">
							<div id="filter-expand-head">
								<span>Choose Your Filter</span>
								<div class="inline-block float-right">
									<i class="toggle-icon fa fa-caret-down"></i>
									<i class="toggle-icon hide fa fa-caret-up"></i>
								</div>
							</div>

							<div id="filter-wrapper">
								<div id="filter-expand">
									<div class="row">
										<div class="col-md-6 col-12">
											<h4 id="filter-header" class="mobile-hide uppercase">Filters</h4>
										</div>
										<div class="col-md-6 col-12 text-right">
											<a id="reset-filter" class="pointer uppercase">Reset</a>
										</div>
									</div>

									<table class="table">
										<!-- <tr>
											<td class="uppercase filter-label-instance">
												Local Tour Operator
											</td>
											<td>
												<div class="custom-control custom-checkbox">
			                                        <input name="local_tour_operator" type="hidden" value="0">
			                                        <input name="local_tour_operator" id="local_tour_operator" type="checkbox" class="custom-control-input" value="1">
			                                        <label class="custom-control-label" for="local_tour_operator"></label>
			                                    </div>
											</td>
										</tr>

										<tr>
											<td class="uppercase filter-label-instance">
												Sustainable Tour Operator
											</td>
											<td>
												<div class="custom-control custom-checkbox">
			                                        <input name="sustainable_tour_operator" type="hidden" value="0">
			                                        <input name="sustainable_tour_operator" id="sustainable_tour_operator" type="checkbox" class="custom-control-input" value="1">
			                                        <label class="custom-control-label" for="sustainable_tour_operator"></label>
			                                    </div>
											</td>
										</tr> -->

										<tr title="Has existing third-party reviews">
											<td class="uppercase filter-label-instance">
												With Reviews
											</td>
											<td>
												<div class="custom-control custom-checkbox">
			                                        <input name="with_reviews" type="hidden" value="0">
			                                        <input <?php echo (isset($input_data['with_reviews']) && $input_data['with_reviews'] == 1) ? "checked" : null; ?> name="with_reviews" id="with_reviews" type="checkbox" class="custom-control-input" value="1">
			                                        <label class="custom-control-label" for="with_reviews"></label>
			                                    </div>
											</td>
										</tr>


										<tr title="Has been verified with business license, permit, etc...">
											<td class="uppercase filter-label-instance">
												Verified
											</td>
											<td>
												<div class="custom-control custom-checkbox">
			                                        <input name="is_verified" type="hidden" value="0">
			                                        <input <?php echo (isset($input_data['is_verified']) && $input_data['is_verified'] == 1) ? "checked" : null; ?> name="is_verified" id="is_verified" type="checkbox" class="custom-control-input" value="1">
			                                        <label class="custom-control-label" for="is_verified"></label>
			                                    </div>
											</td>
										</tr>

										<tr title="Has valid third-party accreditations">
											<td class="uppercase filter-label-instance">
												With Accreditations
											</td>
											<td>
												<div class="custom-control custom-checkbox">
			                                        <input name="with_accreditations" type="hidden" value="0">
			                                        <input <?php echo (isset($input_data['with_accreditations']) && $input_data['with_accreditations'] == 1) ? "checked" : null; ?> name="with_accreditations" id="with_accreditations" type="checkbox" class="custom-control-input" value="1">
			                                        <label class="custom-control-label" for="with_accreditations"></label>
			                                    </div>
											</td>
										</tr>
									</table>

									<input type="submit" class="orange-button filter-trigger pointer" value="FILTER">
								</div>
							</div>

						</div>
						<div class="col-md-9 col-12">
							<div class="row">
								<?php

									foreach($tour_operators as $tour_operator_instance) {
										?>
											<div class="col-md-4 col-12 operator-card">
												<a class="website-link card-instance" href="/directory/{{ $tour_operator_instance->slug }}">
													<div class="card-content-wrapper">
														<div style="background-image: url('/uploads/<?php echo $tour_operator_instance->public_id ."-uploads/". $tour_operator_instance->hero_image; ?>')" class="card-header">	
															<div class="card-logo">
																<img src="/uploads/<?php echo $tour_operator_instance->public_id ."-uploads/". $tour_operator_instance->logo; ?>" />
															</div>
														</div>
														
														<div class="card-body">
															<div class="operator-name">	
																<div class="row">	
																	<div class="col-9">
																		<?php echo $tour_operator_instance->name; ?>
																	</div>
																	<div class="col-3">
																		<?php if($tour_operator_instance->isVerified) { ?>
																			<div class="verified-tag">
																				<i class="fa fa-check"></i>
																			</div>
																		<?php } ?>
																	</div>
																</div>
															</div>
															<div class="operator-excerpt">
																<?php
																	
																	$str = strip_tags($tour_operator_instance->description);
																	if (strlen($str) > 10)
		   																$str = substr($str, 0, 140) . '...';
																	
																	echo $str;
																?>
															</div>
														</div>

														<div class="card-footer">
															
															<?php
																$searchWords = array("https://","http://","/");
																$str = str_ireplace($searchWords,"",$tour_operator_instance->website); 
																echo $str;
															?>

															<br />

															<span class="tour_countries">
																<?php
																	$countries_array = array();
																	$countries = $tour_operator_instance->countries;
																	
																	if(count($countries) > 0) {
																		foreach($countries as $country) {
																			$countries_array[] = $country->country;
																		}

																		echo implode(" | ", $countries_array);
																	}
																?>
															</span>
														</div>
													</div>
												</a>
											</div>
										<?php
									}

								?>
								</div>

								<div class="row">
									<div class="col-12">
										<center>

									    	{{ 
									    		$tour_operators
													->appends([
														'selected_mode' 			=> $selected_mode,
														'countries' 				=> $countries,
														'company_name' 				=> (isset($input_data['company_name'])) ? $input_data['company_name'] : null,
														'local_tour_operator'		=> (isset($input_data['local_tour_operator'])) ? $input_data['local_tour_operator'] : null,
													    'sustainable_tour_operator'	=> (isset($input_data['sustainable_tour_operator'])) ? $input_data['sustainable_tour_operator'] : null,
													    'with_reviews'				=> (isset($input_data['with_reviews'])) ? $input_data['with_reviews'] : null,
													    'is_verified'				=> (isset($input_data['is_verified'])) ? $input_data['is_verified'] : null,
													    'with_accreditations'		=> (isset($input_data['with_accreditations'])) ? $input_data['with_accreditations'] : null
													])
												->links()
									    	}}
									    </center>
									</div>
								</div>

							</div>
						</div>
					</div>
					
				</div>
			</div>
		</form>

		@include('footer') 

	</body>

	<link rel="stylesheet" type="text/css" href="/assets/chosen/chosen.min.css" />
	<script type="text/javascript" src="/assets/chosen/chosen.jquery.min.js"></script>

</html>

<script type="text/javascript">
	$(document).ready( function () {

		$('#reset-filter').click( function () {
			$('.custom-control-input').prop('checked', false);
			$('.custom-control-input').find('label').removeClass('checked');
			$('.filter-trigger').click();
		});

		$('#filter-expand-head').click( function () {

			if($('#filter-wrapper').hasClass('expand')) {
				$('#filter-wrapper').removeClass('expand');

				$('.toggle-icon').addClass('hide');
				$('.toggle-icon.fa-caret-down').removeClass('hide');

			} else {
				$('#filter-wrapper').addClass('expand');

				$('.toggle-icon').addClass('hide');
				$('.toggle-icon.fa-caret-up').removeClass('hide');
			}

		});

		$(".chosen-select").chosen();

		$('.switcher-instance').click( function () {
			$('.switcher-instance').removeClass('active-switch');
			$(this).addClass('active-switch');

			$('.search-toggle-wrapper').hide();
			var target_dom = $(this).data('target');
			var value_switch = $(this).data('value');
			$(target_dom).show();

			$('#selected_mode').val(value_switch);
		});
		
		<?php
			if(isset($input_data['selected_mode']) && $input_data['selected_mode'] == "company") {
				?>

					$('#company-switcher').click();

				<?php
			}
		?>
	});
</script>