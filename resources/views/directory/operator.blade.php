<!DOCTYPE html>
<html>
	@include('headtag', ['title' => $data['tour_operator']['name']])
<body>

	@include('header')

	<?php
		$isMob = false;
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			$isMob = true;
		}
	?>

	<?php if($isMob) { ?>

		<a class="return-link" href="/"><h6 class="inline-block"><i class="fa fa-angle-double-left"></i>&nbsp;<strong>Back To Directory</strong></h6></a>

		<div id="profile-top-header">
			<div style="background-image: url('/uploads/<?php echo $data['tour_operator']->public_id ."-uploads/". $data['tour_operator']->hero_image; ?>')" class="card-header">	
				<div class="card-logo">
					<img src="/uploads/<?php echo $data['tour_operator']->public_id ."-uploads/". $data['tour_operator']->logo; ?>" />
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="limiter">
		<div class="main-content-wrapper">
			<div id="operator-main-info">
				
				<?php if(!$isMob) { ?>				
					<a class="return-link" href="/"><h5 class="inline-block"><i class="fa fa-angle-double-left"></i>&nbsp;<strong>Back To Directory</strong></h5></a>
					<br />
				<?php } ?>

				<div class="row">
					<div class="col-md-5 col-12">
						<div class="relative-wrapper">
							<img class="invisible" src="/uploads/<?php echo $data['tour_operator']['public_id'] ."-uploads/". $data['tour_operator']->hero_image; ?>" /> 	

							<?php if(!$isMob) { ?>
								<div class="absolute-top mobile-relative full-width">
									<img id="operator-logo" src="/uploads/<?php echo $data['tour_operator']['public_id'] ."-uploads/". $data['tour_operator']->logo; ?>" />
								</div>
							<?php } ?>
							
							<div class="absolute-bottom mobile-relative full-width">
								<h1 id="operator_name">{{ $data['tour_operator']['name'] }}</h1>
								<div class="row">
									<div class="col-12">
										<div id="operator-website-info">
											<a class="website-link" target="_blank" href="{{ $data['tour_operator']['website'] }}">
												<?php
													$searchWords = array("https://","http://","/");
													$str = str_ireplace($searchWords,"",$data['tour_operator']['website']); 
													echo $str;
												?>
											</a>
										</div>
									</div>
									<div class="col-12">
										<div id="operator-social-media">

											<?php if(!empty($data['tour_operator']['facebook_link'])) { ?>
												<div title="Facebook" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['facebook_link'] }}"><i class="fab fa-facebook-f"></i></a></div>
											<?php } ?>

											<?php if(!empty($data['tour_operator']['youtube_link'])) { ?>
												<div title="Youtube" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['youtube_link'] }}"><i class="fab fa-youtube"></i></a></div>
											<?php } ?>
											
											<?php if(!empty($data['tour_operator']['twitter_link'])) { ?>
												<div title="Twitter" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['twitter_link'] }}"><i class="fab fa-twitter"></i></a></div>
											<?php } ?>
											
											<?php if(!empty($data['tour_operator']['instagram_link'])) { ?>
												<div title="Instagram" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['instagram_link'] }}"><i class="fab fa-instagram"></i></a></div>
											<?php } ?>
											
											<?php if(!empty($data['tour_operator']['linkedin_link'])) { ?>
												<div title="LinkedIn" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['linkedin_link'] }}"><i class="fab fa-linkedin"></i></a></div>
											<?php } ?>
											
											<?php if(!empty($data['tour_operator']['pinterest_link'])) { ?>
												<div title="Pinterest" class="inline-block"><a target="_blank" href="{{ $data['tour_operator']['pinterest_link'] }}"><i class="fab fa-pinterest"></i></a></div>
											<?php } ?>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7 col-12">
						<?php if(!$isMob) { ?>
							<img id="operator-hero-image" src="/uploads/<?php echo $data['tour_operator']['public_id'] ."-uploads/". $data['tour_operator']->hero_image; ?>" /> 
						<?php } ?>
					</div>
				</div>
			</div>

			<div id="operator-basic-info">
				<h1 class="underline-highlight">
					About Us
					<div class="border-highlight"></div>
				</h1>

				<div class="row">
					<div class="col-md-4 col-12 info-label">Company Details</div>
					<div class="col-md-8 col-12 info-content">
						{{ $data['tour_operator']['company_contact'] }}<br />
						{{ $data['tour_operator']['company_email'] }}<br />
						{{ $data['tour_operator']['company_address'] }}
					</div>
				</div>

				<div class="row">
					<div class="col-md-4 col-12 info-label">Countries where we offer tours</div>
					<div class="col-md-8 col-12 info-content">
						{{ $data['tour_operator']['countries'] }}
					</div>
				</div>


				<div class="row">
					<div class="col-md-4 col-12 info-label">Types of tours offered</div>
					<div class="col-md-8 col-12 info-content">
						<?php echo $data['tour_operator']['tour_types']; ?>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4 col-12 info-label">Description</div>
					<div class="col-md-8 col-12 info-content">
						<?php echo $data['tour_operator']['description']; ?>
					</div>
				</div>

				<?php if(!empty($data['tour_operator']['accreditations'])) { ?>
					<div class="row">
						<div class="col-md-4 col-12 info-label">Accreditations</div>
						<div class="col-md-8 col-12 info-content">
							<?php echo $data['tour_operator']['accreditations']; ?>
						</div>
					</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-4 col-12 info-label">Headquarters</div>
					<div class="col-md-8 col-12 info-content">
						<?php echo $data['tour_operator']['headquarters']; ?>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4 col-12 info-label">Contact Person</div>
					<div class="col-md-8 col-12 info-content">
						<?php echo $data['tour_operator']['contact_person']; ?>
					</div>
				</div>
			</div>

			<div id="awards-holder">
				
				<?php if($data['tour_operator']['world_travel_award'] || $data['tour_operator']['world_travel_award']) { ?>

					<h1 class="underline-highlight">
						Awards
						<div class="border-highlight"></div>
					</h1>
				
				<?php } ?>

				<?php if($data['tour_operator']['world_travel_award']) { ?>
					<div class="award-icon-instance">
						<img src="/assets/icons/world-travel-award.png" />
					</div>
				<?php } ?>

				<?php if($data['tour_operator']['world_travel_award']) { ?>
					<div class="award-icon-instance">
						<img src="/assets/icons/trip-advisor-excellence.png" />
					</div>
				<?php } ?>
			</div>

			
			<div id="reviews-holder">
				<h1 class="underline-highlight">
					Reviews
					<div class="border-highlight"></div>
				</h1>

				<?php if(!empty($data['tour_operator']['google_review_link'])) { ?>
					<a target="_blank" href="{{ $data['tour_operator']['google_review_link'] }}">
						<div class="review-icon-instance">
							<img src="/assets/icons/google-review.png" />
						</div>
					</a>
				<?php } ?>

				<?php if(!empty($data['tour_operator']['trip_advisor_review_link'])) { ?>
					<a target="_blank" href="{{ $data['tour_operator']['trip_advisor_review_link'] }}">
						<div class="review-icon-instance">
							<img src="/assets/icons/trip-advisor.png" />
						</div>
					</a>
				<?php } ?>

				<?php if(!empty($data['tour_operator']['safari_bookings_review_link'])) { ?>
					<a target="_blank" href="{{ $data['tour_operator']['safari_bookings_review_link'] }}">
						<div class="review-icon-instance">
							<img src="/assets/icons/safaribookings.png" />
						</div>
					</a>
				<?php } ?>

				<?php if(!empty($data['tour_operator']['trust_pilot_review_link'])) { ?>
					<a target="_blank" href="{{ $data['tour_operator']['trust_pilot_review_link'] }}">
						<div class="review-icon-instance">
							<img src="/assets/icons/trustpilot.png" />
						</div>
					</a>
				<?php } ?>

			</div>
		</div>
	</div>

	@include('footer') 

</body>
</html>

<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>